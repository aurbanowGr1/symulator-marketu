import generators.SymulatorCsvReader;
import generators.CustomConfig;
import generators.Generator;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.bean.BeanToCsv;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;

import model.*;

public class Program {

	public static void main(String[] args) {
		List<Cashier> cashiers = SymulatorCsvReader.getCashiers();
		
		int iterations = 100000;
		if(args != null && args.length>0)
			iterations = Integer.parseInt(args[0]);
		for(int i = 0;i<iterations; i++){
			Customer newCustomer = SymulatorCsvReader.generateRandomCustomer();
			if(newCustomer != null){
				newCustomer.setId(i);
				Cashier cashier = newCustomer.getBest(getRandomCashiers(cashiers));
				if(cashier==null)
					cashier = cashiers.get(0);
				newCustomer.setCashier(cashier);
			}
			for(Cashier cashier:cashiers){
				cashier.process();
			}
			System.out.println(i);
		}
		
		List<Customer> result = new ArrayList<Customer>();
		for(Cashier cashier:cashiers){
			result.addAll(cashier.getProcessedCustomers());
			
		}
	
		
//		List<Cashier> cashiers = SymulatorCsvReader.getCashiers();
//		
//		List<CustomConfig> configs = Generator.getConfigs();
//		
//		ColumnPositionMappingStrategy<CustomConfig> strategy = 
//        		new ColumnPositionMappingStrategy<CustomConfig>();
//        strategy.setColumnMapping(CustomConfig.columns);
//        BeanToCsv<CustomConfig> bean = new BeanToCsv<CustomConfig>();
//        strategy.setType(CustomConfig.class);
//        
//        try(CSVWriter reader =new CSVWriter( new FileWriter("configs.csv"), ';','"')){
//        	bean.write(strategy, reader, configs);
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}
		
		
		ColumnPositionMappingStrategy<Customer> strategy = 
        		new ColumnPositionMappingStrategy<Customer>();
        strategy.setColumnMapping(Customer.columns);
        BeanToCsv<Customer> bean = new BeanToCsv<Customer>();
        strategy.setType(Customer.class);
        
        try(CSVWriter reader =new CSVWriter( new FileWriter("result.csv"), ';','"')){
        	bean.write(strategy, reader, result);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}
	
	static List<Cashier> getRandomCashiers(List<Cashier> cachiers){
		List<Cashier> result = new ArrayList<Cashier>();
		int length = cachiers.size()-1;
		int i = new Random().nextInt(length);
		int start = i-2<0?0:i-2;
		int end = i+2>=length?length:i+2;
		for(int j=start;j<=end;j++){
			result.add(cachiers.get(j));
		}
		return result;
	}

}


