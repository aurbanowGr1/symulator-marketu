package generators;

import java.util.Random;

import com.opencsv.bean.CsvBind;

import model.Cashier;
import model.Customer;
import model.Gender;

public class CustomConfig {

	public final static String[] columns=new String[]{"id", "ageFrom", "ageTo", "gender", "modificator","forCustomer", "forCashier","forRandomCustomerGeneration","relatedConfigId", "chances"};
	
	
	public CustomConfig() {
	}
	
	
	public CustomConfig(int id, int ageFrom, int ageTo, String gender,
			double modificator, boolean forCustomer, boolean forCashier,
			boolean forRandomCustomerGeneration, int relatedConfigId,
			double chances, CustomConfig relatedConfig) {
		super();
		this.id = id;
		this.ageFrom = ageFrom;
		this.ageTo = ageTo;
		this.gender = gender;
		this.modificator = modificator;
		this.forCustomer = forCustomer;
		this.forCashier = forCashier;
		this.forRandomCustomerGeneration = forRandomCustomerGeneration;
		this.relatedConfigId = relatedConfigId;
		this.chances = chances;
		RelatedConfig = relatedConfig;
	}


	@CsvBind
	private int id;
	@CsvBind
	private int ageFrom;
	@CsvBind
	private int ageTo;
	@CsvBind
	private String gender;
	@CsvBind
	private double modificator;
	@CsvBind
	private boolean forCustomer;
	@CsvBind
	private boolean forCashier;
	@CsvBind
	private boolean forRandomCustomerGeneration;
	@CsvBind
	private int relatedConfigId = -1;
	@CsvBind
	private double chances=1;
	
	private CustomConfig RelatedConfig;

	public boolean CanByAplpliedOn(Cashier cashier){
		return forCashier 
				&&cashier.getAge()>=ageFrom
				&&cashier.getAge()<=ageTo 
				&&cashier.getGender().equalsIgnoreCase(gender);
	}

	public boolean CanByAplpliedOn(Customer customer){
		boolean result =  forCustomer
				&&customer.getAge()>=ageFrom
				&&customer.getAge()<=ageTo 
				&&customer.getGender().equalsIgnoreCase(gender) ;
				
				
				return result;
	}

	public void applyOn(Cashier cashier){
		if(!CanByAplpliedOn(cashier))
			return;
		double speed = cashier.getOperatingSpeed() + modificator;
		double r = new Random().nextDouble();
		if(r<=chances)
			cashier.setOperatingSpeed(speed);
	}

	public void applyOn(Customer customer){
		if(!CanByAplpliedOn(customer))
			return;
		double r = new Random().nextDouble();
		if(r<=chances)
			customer.getConfigs().add(this);
	}
	
	
	
	public int getAgeFrom() {
		return ageFrom;
	}
	public void setAgeFrom(int ageFrom) {
		this.ageFrom = ageFrom;
	}
	public int getAgeTo() {
		return ageTo;
	}
	public void setAgeTo(int ageTo) {
		this.ageTo = ageTo;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public double getModificator() {
		return modificator;
	}
	public void setModificator(double speedModificator) {
		this.modificator = speedModificator;
	}

	public boolean isForCustomer() {
		return forCustomer;
	}

	public void setForCustomer(boolean forCustomer) {
		this.forCustomer = forCustomer;
	}

	public boolean isForCashier() {
		return forCashier;
	}

	public void setForCashier(boolean forCashier) {
		this.forCashier = forCashier;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRelatedConfigId() {
		return relatedConfigId;
	}

	public void setRelatedConfigId(int relatedConfigId) {
		this.relatedConfigId = relatedConfigId;
	}

	public double getChances() {
		return chances;
	}

	public void setChances(double chances) {
		this.chances = chances;
	}
	

	public CustomConfig getRelatedConfig() {
		return RelatedConfig;
	}

	public void setRelatedConfig(CustomConfig relatedConfig) {
		RelatedConfig = relatedConfig;
	}

	public boolean isForRandomCustomerGeneration() {
		return forRandomCustomerGeneration;
	}

	public void setForRandomCustomerGeneration(boolean forRandomCustomerGeneration) {
		this.forRandomCustomerGeneration = forRandomCustomerGeneration;
	}
	
}
