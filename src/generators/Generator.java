package generators;

import java.util.ArrayList;
import java.util.List;

import model.Cashier;
import model.Gender;
import model.Person;

public class Generator {

	public static List<Cashier> getCashiers(){
		List<Cashier> result = new ArrayList<Cashier>();
		for(int i = 0;i<12;i++){
			Cashier cashier = new Cashier();
			cashier.setId(i);
			cashier.setAge(30);
			cashier.setGender("M");
			result.add(cashier);
		}
		return result;
	}
	
	public static List<CustomConfig> getConfigs(){
		List<CustomConfig> result = new ArrayList<CustomConfig>();
		for(int i=0;i<10;i++){
			CustomConfig cfg = new CustomConfig();
			cfg.setId(i);
			cfg.setAgeFrom(30);
			cfg.setAgeTo(40);
			cfg.setForCashier(true);
			cfg.setGender("M");
			
			result.add(cfg);
		}
		for(int i=0;i<5;i++){
			CustomConfig cfg = new CustomConfig();
			cfg.setId(i+10);
			cfg.setAgeFrom(30);
			cfg.setAgeTo(40);
			cfg.setForCustomer(true);
			cfg.setGender("M");
			cfg.setRelatedConfigId(i);
			result.add(cfg);
		}
		return result;
	}
	
	
	
}
