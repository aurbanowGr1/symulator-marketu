package generators;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.bean.BeanToCsv;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;

import model.Cashier;
import model.Customer;
import model.Gender;

public class SymulatorCsvReader {

	public static List<CustomConfig> Configurations;
	
	public static CustomConfig Default(){
		 
		CustomConfig result =new CustomConfig(-1,16,67, "F" ,0.0,false, false, true,-1,1,null );
		double r = new Random().nextDouble();
		if(r<0.5)
			result.setGender("M");
		return result;
	} 
	
	public static List<Cashier> getCashiers(){
		List<Cashier> result = null;
		ColumnPositionMappingStrategy<Cashier> strategy = 
        		new ColumnPositionMappingStrategy<Cashier>();
        String[] columns = new String[]{"id","age","gender"};
        strategy.setColumnMapping(columns);
        CsvToBean<Cashier> bean = new CsvToBean<Cashier>();
        strategy.setType(Cashier.class);
        
        try(CSVReader reader =new CSVReader( new FileReader("cashiers.csv"), ';','"',1)){
        	result = bean.parse(strategy, reader);
		}catch(Exception ex){
			ex.printStackTrace();
		}
        
        if(result !=null){
        	for(Cashier cashier : result){
        		if(Configurations==null) getConfigs();
        		for(CustomConfig cfg : Configurations){
        			cfg.applyOn(cashier);
        		}
        	}
        }
        
        return result;
	}
	
	public static List<CustomConfig> getConfigs(){

		List<CustomConfig> result = null;
		ColumnPositionMappingStrategy<CustomConfig> strategy = 
        		new ColumnPositionMappingStrategy<CustomConfig>();
        strategy.setColumnMapping(CustomConfig.columns);
        CsvToBean<CustomConfig> bean = new CsvToBean<CustomConfig>();
        strategy.setType(CustomConfig.class);
        
        try(CSVReader reader =new CSVReader( new FileReader("configs.csv"), ';','"',1)){
        	result = bean.parse(strategy, reader);
		}catch(Exception ex){
			ex.printStackTrace();
		}
        for(CustomConfig cfg : result){
        	if(cfg.isForCustomer() && cfg.getRelatedConfigId()>=0){
        		for(CustomConfig c : result){
        			if(c.getId()==cfg.getRelatedConfigId()){
        				cfg.setRelatedConfig(c);
        				break;
        			}
        		}
        	}
        }
        Configurations = result;
        return result;
	}
	
	public static Customer generateRandomCustomer(){
		
		Customer result = new Customer();
		List<CustomConfig> configs = configsForGeneartions();
		if(!configs.isEmpty())
		{
			Random random = new Random();
			double r = random.nextDouble();
			double range = 0;
			CustomConfig config = null;
			for(CustomConfig cfg : configs){
				range+=cfg.getChances();
				if(r<range){
					config = cfg;
					break;
				}
			}
			if(config == null) 
				return null;
			result.setAge(random.nextInt(config.getAgeTo()-config.getAgeFrom())+config.getAgeFrom());
			result.setGender(config.getGender());
			result.setProductsCount(random.nextInt(19)+1);
			for(CustomConfig cfg:Configurations){
				cfg.applyOn(result);
			}
		}
		
		return result;
	}
	
	static List<CustomConfig> configsForGeneartions(){
		if(Configurations==null) getConfigs();
		List<CustomConfig> result = new ArrayList<CustomConfig>();
		for(CustomConfig cfg : Configurations){
			if(cfg.isForRandomCustomerGeneration())
				result.add(cfg);
		}
		
		return result;
	}
	
}


