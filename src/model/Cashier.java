package model;

import java.util.ArrayList;
import java.util.List;

public class Cashier extends Person {

	private List<Customer> customers = new ArrayList<Customer>();
	private List<Customer> processedCustomers = new ArrayList<Customer>();
	
	private double productsInQueue;
	private double operatingSpeed = 1;
	private Customer currentCustomer;
	
	public void addCustomer(Customer customer){
		customers.add(customer);
		productsInQueue+=customer.getProductsCount();
	}
	
	public void removeCustomer(Customer customer){
		customers.remove(customer);
	}
	
	public void process(){
		
		for(Customer customer: customers){
			customer.increaseTimeSpentInQueue();
		}
		if(currentCustomer==null && !customers.isEmpty())
			currentCustomer=customers.get(0);
		if(currentCustomer==null)return;
		productsInQueue-=operatingSpeed;
		if(currentCustomer.getProductsCount()>0){
			currentCustomer.processProducts(operatingSpeed);
		}else{
			removeCustomer(currentCustomer);
			processedCustomers.add(currentCustomer);
			currentCustomer =null;
		}
	}
	
	public double queueCost(double newProducts){
		return (productsInQueue+newProducts) * operatingSpeed;
	}

	public double getOperatingSpeed() {
		return operatingSpeed;
	}

	public void setOperatingSpeed(double operatingSpeed) {
		this.operatingSpeed = operatingSpeed;
	}

	public List<Customer> getProcessedCustomers() {
		return processedCustomers;
	}
	
}
