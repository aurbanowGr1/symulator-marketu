package model;

import generators.CustomConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.opencsv.bean.CsvBind;

public class Customer extends Person {

	public static String[] columns = new String[]{"id", "age", "gender","timeSpentInQueue","products","cashierId"};
	@CsvBind
	private int timeSpentInQueue;
	private double ProductsCount;
	@CsvBind
	private double products;
	private double processedProducts;
	@CsvBind
	private int cashierId;
	private List<CustomConfig> configs = new ArrayList<CustomConfig>();
	
	public void increaseTimeSpentInQueue(){
		timeSpentInQueue++;
	}
	
	public void processProducts(double products){
		processedProducts+=products;
		ProductsCount-=products;
	}
	
	public Cashier getBest(List<Cashier> cashiers){
		Cashier result = null;
		double queueCost = Double.MAX_VALUE;
		
		for(Cashier cashier : cashiers){
			CustomConfig cfg = null;
			for(CustomConfig c: configs){
				if(c.getRelatedConfig()!=null && c.getRelatedConfig().CanByAplpliedOn(cashier)){
					cfg = c;
					break;
				}
			}
			
			double temp = 0;
			temp = countQueueCost(cashier);
			if(temp<=queueCost){
				if(result==null)
					result = cashier;
				else if(temp<queueCost)
						result=cashier;
					else if(cfg!=null && !cfg.getRelatedConfig().CanByAplpliedOn(result)){
						double r= new Random().nextDouble();
						if(r<cfg.getChances())
							result =cashier;
					}
				queueCost=temp;
			}
		}
		return result;
		
	}
	
	public double countQueueCost(Cashier cashier){
	
		double modificator = 1;
		return cashier.queueCost(ProductsCount)*modificator;
	}
	
	private Cashier cashier;
	
	public int getTimeSpentInQueue() {
		return timeSpentInQueue;
	}
	public void setTimeSpentInQueue(int timeSpentInQueue) {
		this.timeSpentInQueue = timeSpentInQueue;
	}
	public double getProductsCount() {
		return ProductsCount;
	}
	public void setProductsCount(double productsCount) {
		ProductsCount = productsCount;
		this.products=productsCount;
	}
	public Cashier getCashier() {
		return cashier;
	}
	public void setCashier(Cashier cashier) {
		this.cashier = cashier;
		cashier.addCustomer(this);
		cashierId=cashier.getId();
	}
	public double getProcessedProducts() {
		return processedProducts;
	}
	public void setProcessedProducts(double processedProducts) {
		this.processedProducts = processedProducts;
	}

	public List<CustomConfig> getConfigs() {
		return configs;
	}

	public int getCashierId() {
		return cashierId;
	}

	public void setCashierId(int cashierId) {
		this.cashierId = cashierId;
	}

	public double getProducts() {
		return products;
	}

	public void setProducts(double products) {
		this.products = products;
	}
	
}
