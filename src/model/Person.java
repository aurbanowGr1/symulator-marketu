package model;

import com.opencsv.bean.CsvBind;

public class Person {

	@CsvBind(required=true)
	private int id;
	@CsvBind(required=true)
	private int age;
	@CsvBind(required=true)
	private String gender;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
	
}
